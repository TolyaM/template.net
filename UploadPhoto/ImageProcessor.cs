﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;


namespace ParkSolo.WEB_2_1.Helpers
{
    /// <summary>
    /// The class represent facade pattern for the ImageSharp library, the class makes it easy to work with images
    /// </summary>
    internal class ImageProcessor : IImageProcessor
    {
        public IEnumerable<string> SupportedMimeTypes => new[] {"image/jpeg", "image/jpg", "image/png"};


        /// <summary>
        /// Get mime type of images from the byte stream
        /// </summary>
        /// <param name="stream">the byte array of the image</param>
        /// <returns>the mime type of the provided byte array</returns>
        public string GetImageMimeType(byte[] stream)
        {
            IImageFormat format;

            using (var image = Image.Load(stream, out format))
            {
                return format.DefaultMimeType;
            }
        }


        /// <summary>
        /// Get image extension of the provided byte array 
        /// </summary>
        /// <param name="stream">byte array of the image</param>
        /// <returns>file extension</returns>
        public string GetImageExtension(byte[] stream)
        {
            IImageFormat format;

            using (var image = Image.Load(stream, out format))
            {
                return format.FileExtensions.First();
            }
        }


        public bool CheckFileType(IEnumerable<IFormFile> files)
        {
            bool isCorrectType = true;
            if (files != null)
            {
                isCorrectType = !files.Where(x => !SupportedMimeTypes.ToList().Select(t => t)
                                            .Contains(x.ContentType)).Any();
            }
            
            return isCorrectType;
        }
    }
}