//The method loads the map with center in current location, with fields for input start point, finish point and selected value of travelMode
function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var geocoder = new google.maps.Geocoder;    //class for converting address and LatLng
    var positionMap;    //coordinates center map by loading
    var zoomValue = 11; //level of scalability
    var infoWindow = new google.maps.InfoWindow;


    //create map
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoomValue,
        center: positionMap
    });


    //set coordinates center map as current coordinates 
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            positionMap = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(positionMap);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    
    var inputStartRoute = document.getElementById('start');
    var inputEndRoute = document.getElementById('end');

    //get address start point route
    var autocompleteStartRoute = new google.maps.places.Autocomplete(
        inputStartRoute, { placeIdOnly: true });
    //bind start route to map
    autocompleteStartRoute.bindTo('bounds', map);

    //get address end point route
    var autocompleteEndRoute = new google.maps.places.Autocomplete(
        inputEndRoute, { placeIdOnly: true });
    //bind end route to map
    autocompleteEndRoute.bindTo('bounds', map);


    //add listener by change start point of route
    autocompleteStartRoute.addListener('place_changed', function () {
        var placeStartRoute = autocompleteStartRoute.getPlace();
        if (!placeStartRoute.place_id) {
            return;
        }
        //get coordinates start point of route
        geocoder.geocode({ 'placeId': placeStartRoute.place_id }, function (results, status) {

            if (status !== 'OK') {
                window.alert('Geocoder failed due to: ' + status);
                return;
            }
            var coordinateCenterMap = results[0].geometry.location;
            map.setZoom(zoomValue);
            map.setCenter(coordinateCenterMap);
        });
    });

    //add listener by change end point of route
    autocompleteEndRoute.addListener('place_changed', function () {
        var placeEndRoute = autocompleteEndRoute.getPlace();
        if (!placeEndRoute.place_id) {
            return;
        }

        //get coordinates end point of route
        geocoder.geocode({ 'placeId': placeEndRoute.place_id }, function (results, status) {

            if (status !== 'OK') {
                window.alert('Geocoder failed due to: ' + status);
                return;
            }
            var coordinateCenterMap = results[0].geometry.location;
            map.setZoom(zoomValue);
            map.setCenter(coordinateCenterMap);
        });
    });

    var onChangeModeRoute = function () {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    };

    document.getElementById('mode').addEventListener('change', onChangeModeRoute);

    directionsDisplay.setMap(map);
}

//The method return route by 2 points (start and end) and displays it on map
//Parameter directionsService is instance DirectionService() for getting route
//Parameter directionsDisplay is instance DirectionDisplay() for displaying route on map
function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var inputStartRoute = document.getElementById('start');
    var inputEndRoute = document.getElementById('end');

    var origin = inputStartRoute.value;     //start point of route
    var destination = inputEndRoute.value;  //end point of route  
    var selectedMode = document.getElementById('mode').value;   //mode of route
    directionsService.route({
        origin: origin, 
        destination: destination,
        travelMode: google.maps.TravelMode[selectedMode]
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

//The method handles error
//Parameter browserHasGeolocation is flag (browser supports geolocation)
//Parameter infoWindow is instance of InfoWindow (will contains error message)
//Parameter pos is coordinates position of infoWindow
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

