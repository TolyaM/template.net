﻿using SlateCORE.Common.Enums;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SlateCORE.Web.Controllers.API
{
    public class VideoStreamerController: ApiController
    {
        private const string videoFilePath = "~/video/";//resources


        /// <summary>
        /// Method of obtaining video
        /// </summary>
        /// <param name="fileName">Имя файда.</param>
        /// <param name="bufferLength">Перечесление, от которого зависит размер буфера.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/videostreamer/")]
        public async Task<IHttpActionResult> GetLiveVideo(string fileName, BufferLength bufferLength = BufferLength.HD)
        {
            if(String.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException();
            }

            string filePath = Path.Combine(HttpContext.Current.Server.MapPath(videoFilePath), fileName);
            return new VideoFileActionResult(filePath, bufferLength);//вынести в переменную
        }
    }


    /// <summary>
    /// Action Result for Returning Stream
    /// </summary>
    public class VideoFileActionResult : IHttpActionResult
    {
        private readonly long _bufferLength;

        public VideoFileActionResult(string videoFilePath, BufferLength bufferLength)
        {
            this.filepath = videoFilePath;
            _bufferLength = Convert.ToInt32(value: bufferLength);
        }

        public string filepath { get; private set; }


        /// <summary>
        /// Web API calls the ExecuteAsync method to create an HttpResponseMessage. 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// PushStream - Enables scenarios where a data producer wants to write directly (either synchronously or asynchronously) using a stream.
        /// Add new header - ContentLength
        /// <returns>HttpResponseMessage in a new thread.</returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            FileInfo fileInfo = new FileInfo(this.filepath);

            long totalLength = fileInfo.Length;
            response.Content = new PushStreamContent((outputStream, httpContent, transportContext) =>
            {
                OnStreamConnected(outputStream, httpContent, transportContext);
            });

            response.Content.Headers.ContentLength = totalLength;
            return Task.FromResult(response);
        }


        /// <summary>
        /// Returns a stream. Сonnection method to stream video file
        /// </summary>
        /// <param name="outputStream">Output stream returned to the client</param>
        /// <param name="content">A base class representing an HTTP entity body and content headers.</param>
        /// <param name="context">transport layer provider</param>
        /// Open the file for reading. We get its size. While the video size and the size of the bytes for reading are greater than zero, we return the stream of bytes.
        private async void OnStreamConnected(Stream outputStream, HttpContent content, TransportContext context)
        {
            try
            {
                var buffer = new byte[_bufferLength];
                    
                using (var nypdVideo = File.Open(this.filepath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var videoLength = (int)nypdVideo.Length;
                    var videoBytesRead = 1;

                    while (videoLength > 0 && videoBytesRead > 0)
                    {
                        videoBytesRead = nypdVideo.Read(buffer, 0, Math.Min(videoLength, buffer.Length));
                        await outputStream.WriteAsync(buffer, 0, videoBytesRead);
                        videoLength -= videoBytesRead;
                    }
                }
            }
            catch (HttpException ex)
            {
                return;
            }
            finally
            {
                // Close output stream as we are done
                outputStream.Close();
            }
        }
    }
}
