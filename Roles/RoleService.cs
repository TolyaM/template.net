﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Archive.BLL.DTO;
using Archive.BLL.Interfaces;
using Archive.DAL.Entities;
using Archive.DAL.Interfaces;
using AutoMapper;

namespace Archive.BLL.Services
{
    public class RoleService:IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;


        public RoleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Dispose()
        {

        }


        /// <summary>
        /// Get role by id
        /// </summary>
        /// <param name="id">Role Id</param>
        /// <returns>The found role</returns>
        public async Task<RoleDTO> GetById(int? id)
        {
            if (!id.HasValue)
            {
                throw new NotImplementedException();
            }

            var role = await _unitOfWork.RoleRepository.GetByID(id.Value);
            var roleDto = _mapper.Map<Role, RoleDTO>(role);

            return roleDto;
        }


        /// <summary>
        /// Creates role and save it in database
        /// </summary>
        /// <param name="item">role</param>
        public async Task Create(RoleDTO item)
        {
            var roleExist = await _unitOfWork.RoleRepository.GetByPredicate(s => s.CompanyId == item.CompanyId && s.Name == item.Name);
            if (roleExist.Any())
            {
                throw new NotImplementedException();
            }

            var role = _mapper.Map<RoleDTO, Role>(item);
            role.CreatedAt = DateTime.Now;

            await _unitOfWork.RoleRepository.Create(role);
            _unitOfWork.Commit();
        }



        /// <summary>
        /// Creates role and save it in database
        /// </summary>
        /// <param name="item">role</param>
        public async Task Create(RoleCreateDTO item)
        {
            var roleExist = await _unitOfWork.RoleRepository.GetByPredicate(s => s.CompanyId == item.CompanyId && s.Name == item.Name);
            if (roleExist.Any())
            {
                throw new NotImplementedException();
            }

            var role = _mapper.Map<RoleCreateDTO, Role>(item);
            role.CreatedAt = DateTime.Now;

            await _unitOfWork.RoleRepository.Create(role);


            var permissionRoles = new List<PermissionRole>();

            foreach (var permission in item.PermissionDtos)
            {
                permissionRoles.Add(new PermissionRole()
                {
                    PermissionId = permission.Id,
                    RoleId = role.Id
                });
            }


            await _unitOfWork.PermissionRoleRepository.BulkInsert(permissionRoles);
            _unitOfWork.Commit();
        }


        /// <summary>
        /// Update role and save it in db
        /// </summary>
        /// <param name="roleEditDto">The provided role</param>
        public async Task Update(RoleEditDTO roleEditDto)
        {
            var role = await _unitOfWork.RoleRepository.GetByID(roleEditDto.Id);

            var editedRole = _mapper.Map<RoleEditDTO, Role>(roleEditDto);

            editedRole.CreatedAt = role.CreatedAt;
            editedRole.CompanyId = role.CompanyId;
            editedRole.Name = roleEditDto.Name;

            await _unitOfWork.RoleRepository.Update(editedRole);



            var permissionRoles = new List<PermissionRole>();

            foreach (var permission in roleEditDto.PermissionDtos)
            {
                permissionRoles.Add(new PermissionRole()
                {
                    PermissionId = permission.Id,
                    RoleId = role.Id
                });
            }


            await _unitOfWork.PermissionRoleRepository.Delete(s => s.RoleId == role.Id);
            await _unitOfWork.PermissionRoleRepository.BulkInsert(permissionRoles);

            _unitOfWork.Commit();
        }


        /// <summary>
        /// Gets all roles from database by company id
        /// </summary>
        /// <param name="companyId">company Id</param>
        public async Task<IEnumerable<RoleDTO>> GetAll(int? companyId)
        {
            if (companyId==null)
            {
                throw new NotImplementedException();
            }

            var roles = await _unitOfWork.RoleRepository.GetByPredicate(s=>s.IsDeleted != true && s.CompanyId==companyId);
            var rolesDto = _mapper.Map<IEnumerable<Role>, IEnumerable<RoleDTO>>(roles);

            return rolesDto;
        }


        /// <summary>
        /// Get role with permission by role Id
        /// </summary>
        /// <param name="roleId">Role id</param>
        /// <returns>Found role</returns>
        public async Task<RoleFullDTO> GetRoleWithPermissions(int roleId)
        {
            var role = (await _unitOfWork.RoleRepository.GetByPredicate(s => s.Id == roleId)).FirstOrDefault();
            var roleDto = _mapper.Map<Role, RoleFullDTO>(role);

            var permissionRoles = await _unitOfWork.PermissionRoleRepository.GetByPredicate(s => s.RoleId == roleId);

            var permissions = (await _unitOfWork.PermissionRepository.GetAll()).Where(x=> permissionRoles.Select(f=>f.PermissionId).Contains(x.Id)).ToList();
            var permissionDto = _mapper.Map<IEnumerable<Permission>, IEnumerable<PermissionDTO>>(permissions);

            roleDto.Permissions = permissionDto;

            return roleDto;
        }


        /// <summary>
        /// The method delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(int? id)
        {
            if (!id.HasValue)
            {
                throw new NotImplementedException();
            }

            var users = await _unitOfWork.UserRepository.GetByPredicate(s => s.RoleId == id.Value && s.IsDeleted!=true);

            if (users.Any())
            {
                throw new NotImplementedException();
            }

            var role = await _unitOfWork.RoleRepository.GetByID(id.Value);
            role.IsDeleted = true;

            await _unitOfWork.RoleRepository.Update(role);
            _unitOfWork.Commit();
        }
    }
}