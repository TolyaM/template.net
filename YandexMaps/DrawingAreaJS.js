ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
        center: [55.73, 37.75],
        zoom: 10
    },
        {
        searchControlProvider: 'yandex#search'
    });

    myPolygon = createPolygon();
    // ��������� ������������� �� �����.
    myMap.geoObjects.add(myPolygon);

    changeColorBorder(myPolygon);

    // �������� ����� �������������� � ������������ ���������� ����� ������.
    myPolygon.editor.startDrawing();
    var button = document.getElementById('button');
    button.addEventListener('click', function () {
        var coordinates = JSON.stringify(myPolygon.geometry._coordPath._coordinates);
        debugger;
        alert(coordinates);
        saveCoordinates(coordinates, "coordinates.json");
    });
}

function createPolygon() {
    // ������� ������������� ��� ������.
    var myPolygon = new ymaps.Polygon([], {}, {
        // ������ � ������ ���������� ����� ������.
        editorDrawingCursor: "crosshair",
        // ����������� ���������� ���������� ������.
        editorMaxPoints: 5,
        // ���� �������.
        strokeColor: '#76ff76',
        // ������ �������.
        strokeWidth: 1
    });

    return myPolygon;
}

function changeColorBorder(myPolygon) {
    // � ������ ���������� ����� ������ ������ ���� ������� ��������������.
    var stateMonitor = new ymaps.Monitor(myPolygon.editor.state);
    stateMonitor.add("drawing", function (newValue) {
        myPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
    });
}

//The method save text in json-file
//Parameter text is stored data
//Parameter filename is name of new file
function saveCoordinates(text, filename) {
    debugger 
    var a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click();
}
