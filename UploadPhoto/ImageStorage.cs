﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace ParkSolo.WEB_2_1.Helpers
{
    public class ImageStorage : IImageStorage
    {
        private readonly AppSettings settings;
        private readonly IHostingEnvironment env;


        public ImageStorage(IHostingEnvironment environment, IOptions<AppSettings> options)
        {
            this.settings = options.Value;
            this.env = environment;
        }


        /// <summary>
        /// Save to image to disc
        /// </summary>
        /// <param name="parkingPlaceId">The Id of parking place</param>
        /// <param name="stream">byte array of provided image</param>
        /// <param name="fileName">Name of image</param>
        /// <returns>file name</returns>
        public async Task<string> SaveAsync(int parkingPlaceId, byte[] stream, string fileName)
        {
            var fileStoragePath = Path.Combine(env.ContentRootPath, settings.FileStoragePath, parkingPlaceId.ToString());
            EnsureDirExists(fileStoragePath);

            using (var fileStream = new FileStream(Path.Combine(fileStoragePath, fileName), FileMode.Create))
            {
                await fileStream.WriteAsync(stream, 0, stream.Length);
            }

            return fileName;
        }


        /// <summary>
        /// Load image from
        /// </summary>
        /// <param name="blobPath">path to image</param>
        /// <returns></returns>
        public async Task<byte[]> LoadAsync(string blobPath)
        {
            var path = ExpandBlobPath(blobPath);
            return await File.ReadAllBytesAsync(path);
        }


        /// <summary>
        /// Сounts the number of files found in the folder
        /// </summary>
        /// <param name="parkingPlaceId">The Id of parking place</param>
        /// <returns>number of files</returns>
        public int FilesCount(int parkingPlaceId)
        {
            var path = ExpandBlobPath(parkingPlaceId.ToString());

            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(path);
            if (!dir.Exists)
            {
                return 0;
            }

            int count = dir.GetFiles().Length;
            return count;
        }


        /// <summary>
        /// Determinates whether the specified file exist
        /// </summary>
        /// <param name="blobPath"></param>
        /// <returns>true if file path exist</returns>
        public bool Exists(string blobPath)
        {
            var path = ExpandBlobPath(blobPath);

            return System.IO.File.Exists(path);
        }


        /// <summary>
        /// Expand path (combine server path and current)
        /// </summary>
        /// <param name="blobPath"></param>
        /// <returns></returns>
        private string ExpandBlobPath(string blobPath)
        {
            return Path.Combine(env.ContentRootPath, settings.FileStoragePath, blobPath);
        }


        /// <summary>
        /// Create folder if folder doesn't exist
        /// </summary>
        /// <param name="fileStoragePath"></param>
        private void EnsureDirExists(string fileStoragePath)
        {
            if (!Directory.Exists(fileStoragePath))
            {
                Directory.CreateDirectory(fileStoragePath);
            }
        }
    }
}