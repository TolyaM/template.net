﻿using SlateCore.BLL.Helpers;
using SlateCore.BLL.Interfaces.Providers;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// Class for working with xml documents
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    internal class XmlProvider<T>: IXmlProvider<T> where T: class
    {
        private readonly IValidationHelper _validationHelper;

        public XmlProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }

        /// <summary>
        /// Method to import data from xml file to object (array of objects)
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>Deserialized objects</returns>
        public T[] ImportArray(string path)
        {
            _validationHelper.PathValidate(path);   
            _validationHelper.PathExist(path);

            using (StreamReader streamReader = new StreamReader(path))
            using (StringReader stringReader = new System.IO.StringReader(streamReader.ReadToEnd()))
            {
                var serializer = new XmlSerializer(typeof(T[]), new XmlRootAttribute("ArrayOf" + nameof(T)));
                return (T[])serializer.Deserialize(stringReader);
            }
        }


        /// <summary>
        /// Method to import data from xml file to object
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>Deserialized object</returns>
        public T Import(string path)
        {
            _validationHelper.PathValidate(path);
            _validationHelper.PathExist(path);

            using (StreamReader streamReader = new StreamReader(path))
            using (StringReader stringReader = new System.IO.StringReader(streamReader.ReadToEnd()))
            {
                var serializer = new XmlSerializer(typeof(T), new XmlRootAttribute(nameof(T)));
                return (T)serializer.Deserialize(stringReader);
            }
        }


        /// <summary>
        /// The method of exporting objects to the xml file
        /// </summary>
        /// <param name="entities">Collection of objects</param>
        /// <param name="path">The path to the file</param>
        public void Export(IEnumerable<T> entities, string path)
        {
            _validationHelper.EntitiesValidate(entities);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            XmlSerializer serializer = new XmlSerializer(entities.GetType());
            using (TextWriter writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, entities);
            }
        }


        /// <summary>
        /// The method of exporting object to the xml file
        /// </summary>
        /// <param name="entity">Object</param>
        /// <param name="path">The path to the file</param>
        public void Export(T entity, string path)
        {
            _validationHelper.EntityValidate(entity);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            XmlSerializer serializer = new XmlSerializer(entity.GetType());
            using (TextWriter writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, entity);
            }
        }
    }
}
