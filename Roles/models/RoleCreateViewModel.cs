﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Archive.DAL.Entities;
using Archive.PL.Resources;

namespace Archive.PL.Models
{
    public class RoleCreateViewModel
    {
        [Display(Name = "RoleName", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "ErrorMessageRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(maximumLength: 30, MinimumLength = 5, ErrorMessageResourceName = "ErrorMessageRoleLength", ErrorMessageResourceType = typeof(Resource))]
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }

    public class PermissionViewModel
    {
        public String Id { get; set; }
        public String Name { get; set; }

        public bool Selected { get; set; }
    }
}