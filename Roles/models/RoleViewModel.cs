﻿using System;
using System.ComponentModel.DataAnnotations;
using Archive.PL.Resources;

namespace Archive.PL.Models
{
    public class RoleViewModel
    {
        public int Id { get; set; }
        [Display(Name = "RoleName", ResourceType = typeof(Resource))]
        public string Name { get; set; }

        [Display(Name = "RoleCreateAt", ResourceType = typeof(Resource))]
        public DateTime CreatedAt { get; set; }
    }
}