﻿using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using SlateCore.BLL.Interfaces.Providers;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using TheArtOfDev.HtmlRenderer.WinForms;
using Point = System.Drawing.Point;
using Rectangle = System.Drawing.Rectangle;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    ///  Class for working with png documents
    /// </summary>
    internal class PngProvider: IPngProvider
    {
        private readonly IValidationHelper _validationHelper;

        public PngProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }


        /// <summary>
        /// The method of exporting file to the png file
        /// </summary>
        /// <param name="documentPath">The path to the file</param>
        public void ExportDocumentToPng(string documentPath)
        {
            _validationHelper.PathExist(documentPath);

            string extension = Path.GetExtension(documentPath);
            var outputPath = documentPath.Substring(0, documentPath.Length - extension.Length) + ".png";

            var htmlDocument = ConvertDocumentToHtml(documentPath);
            int width = Screen.AllScreens.Select(screen => screen.Bounds)
                .Aggregate(Rectangle.Union)
                .Size.Width;
            int height = Screen.AllScreens.Select(screen => screen.Bounds)
                .Aggregate(Rectangle.Union)
                .Size.Height;
            
            Bitmap bitmap = new Bitmap(width / 2, height / 2);
            Point point = new Point(0, 0);
            Size size = new System.Drawing.Size(width / 2, height / 2);
            HtmlRender.Render(Graphics.FromImage(bitmap), htmlDocument.ToString(), point, size);

            bitmap.Save(outputPath, ImageFormat.Png);
        }


        /// <summary>
        /// The method of converting the document to html file
        /// </summary>
        /// <param name="documentPath"></param>
        /// <returns>Html document</returns>
        private XElement ConvertDocumentToHtml(string documentPath)
        {
            byte[] byteArray = File.ReadAllBytes(documentPath);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                memoryStream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument document = WordprocessingDocument.Open(memoryStream, true))
                {
                    HtmlConverterSettings settings = new HtmlConverterSettings()
                    {
                        PageTitle = "My Page Title"
                    };

                    XElement htmlDocument = HtmlConverter.ConvertToHtml(document, settings);
                    //File.WriteAllText(outputPath, html.ToStringNewLineOnAttributes());
                    return htmlDocument;
                }
            }
        }
    }
}
