﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ParkSolo.BLL.DTO;
using ParkSolo.BLL.Infrastructure;
using ParkSolo.BLL.Interfaces;
using ParkSolo.Common.Enums;
using ParkSolo.WEB_2_1.Controllers;
using Serilog;

namespace ParkSolo.WEB_2_1.Infastructure.Middleware
{
    public class WebSocketMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly WebSocketManager _socketManager;

        public WebSocketMiddleware(RequestDelegate next, WebSocketManager socketManager)
        {
            _next = next;
            _socketManager = socketManager;
        }


        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await _next.Invoke(context);

                return;
            }

            var socket = await context.WebSockets.AcceptWebSocketAsync();

            WebSocketInput inputViewModel = null;
            await Receive(socket, async (result, buffer) =>
            {

                UserAccountDTO foundUser;
                string json;

                try
                {
                    using (var _accountService = (IAccountService) context.RequestServices.GetService(typeof(IAccountService)))
                    {
                        inputViewModel = JsonConvert.DeserializeObject<WebSocketInput>(System.Text.Encoding.Default.GetString(buffer));
                       

                        var jwtToken = new JwtSecurityToken(inputViewModel.Token.Remove(0, 7));
                        var userId = Convert.ToInt32(jwtToken.Claims.FirstOrDefault(s => s.Type == "Id").Value);

                        foundUser = _accountService.GetById(userId).GetAwaiter().GetResult();
                    }

                    if (result.MessageType == WebSocketMessageType.Close)
                    {
                        await _socketManager.RemoveSocket(foundUser.Id);

                        return;
                    }

                    if (foundUser != null)
                    {
                        IEnumerable<NotificationDTO> notifications;

                        using (var notificationService = (INotificationService) context.RequestServices.GetService(typeof(INotificationService)))
                        {
                            TimeConverter tc = new TimeConverter();
                            var dateTimeInUnix = Convert.ToInt32(tc.DateTimeToUnixSeconds(DateTime.Now));

                            notifications =
                                (await notificationService.GetByUserId(foundUser.Id)).Where(s =>
                                    s.CreatedOn < dateTimeInUnix);

                            var owner = notifications.Where(s => s.EventStatus == EventStatus.Owner)
                                .Select(s => s.ParkingId)
                                .Distinct();

                            var tenant = notifications.Where(s => s.EventStatus == EventStatus.Tenant)
                                .Select(s => s.ParkingId)
                                .Distinct();

                            var sum = notifications.Where(s => s.EventStatus == EventStatus.Sum).ToList().Count();


                            var ownerParkingExpire = notifications.Where(s => s.EventStatus == EventStatus.OwnerParkingExpire)
                                .Select(s => s.ParkingId)
                                .Distinct();


                            //TODO: [Eugene Lakhmitski 13.11.2018 added phone number changed property]
                            var model = new WebSocketOutput
                            {
                                IsBlocked = foundUser.IsBlocked,
                                ParkingsTenant = tenant.ToList(),
                                ParkingsOwner = owner.ToList(),
                                MyParkingPlaces = ownerParkingExpire.ToList(),
                                Payment = sum
                            };

                            json = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                            {
                                ContractResolver = new CamelCasePropertyNamesContractResolver()
                            });

                            foreach (var item in notifications)
                            {
                                item.IsSocket = true;
                                await notificationService.Update(item);
                            }
                        }

                        var foundSocket = _socketManager.GetSocketById(foundUser.Id);
                        if (foundSocket!=null)
                        {
                            await _socketManager.RemoveSocket(foundUser.Id);
                        }

                        await _socketManager.SendMessageAsync(socket, json);
                        _socketManager.AddSocket(socket, foundUser.Id);
                    }
                    else
                    {
                        await socket.CloseAsync(WebSocketCloseStatus.InvalidPayloadData, "Invalid user id",
                            CancellationToken.None);
                    }
                }
                catch(Exception)
                {
                    if (socket.State == WebSocketState.Open)
                    {
                        await socket.CloseOutputAsync(WebSocketCloseStatus.InvalidPayloadData, "Something went wrong",
                            CancellationToken.None);

                    }
                }
            });
        }


        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                    cancellationToken: CancellationToken.None);

                handleMessage(result, buffer);
            }
        }
    }
}