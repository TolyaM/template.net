// This example requires the Drawing library. Include the libraries=drawing
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
var positionMap;    //coordinates center map by loading
var zoomValue = 8;  //level of scalability
var map;


//The method loads the map with center in current location, with drawing manager(for drawing polygone on map), with event listner for polygon (polygoncomplete).
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: positionMap,
        zoom: zoomValue
    });

    //set coordinates center map as current coordinates 
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    var drivingManager=setDrawingManager();

    //add listener for drivingManager by 
    google.maps.event.addListener(drivingManager, 'polygoncomplete', function (polygon) {
        var coordinates = JSON.stringify(polygon.getPath().getArray()); //get coordinates polygon
        saveCoordinates(coordinates, "coordinates.json");   //save coordinate in json-file
    });
}

//setup manager for drawing polygons
function setDrawingManager() {
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,    //defines shape for drawing 
        drawingControl: true,                                    //defines the visibility of the drawing tools on map
        drawingControlOptions: {                                 //defines options for the drawing tools
            position: google.maps.ControlPosition.TOP_CENTER,    //set position for drawing tools
            drawingModes: ['polyline']                           //set elements of drawing tools
        }
    });

    drawingManager.setMap(map);
    return drawingManager;
}

//The method save text in json-file
//Parameter text is stored data
//Parameter filename is name of new file
function saveCoordinates(text, filename) {
    var a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click();
}

//The method handles error
//Parameter browserHasGeolocation is flag (browser supports geolocation)
//Parameter infoWindow is instance of InfoWindow (will contains error message)
//Parameter currentLocation is coordinates position of infoWindow
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

