﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ParkSolo.BLL.Infrastructure;
using ParkSolo.BLL.Interfaces;
using ParkSolo.WEB_2_1.Services;
using Quartz;
using Quartz.Spi;
using Quartz.Util;

namespace ParkSolo.WEB_2_1.Controllers
{
    //TODO: DELETE
    [Route("api/[controller]")]
    public class NotificationController : ControllerBase
    {
        private readonly IWebSocketService _webSocketService;
        private readonly IScheduler _scheduler;
        private readonly IJobFactory _jobFactory;
        private readonly INotificationService _notificationService;
        private readonly IAccountService _accountService;
        private readonly IFirebasePushService _firebasePushService;


        public NotificationController(IWebSocketService webSocketService, IScheduler scheduler, IJobFactory jobFactory,
            INotificationService notificationService, IAccountService accountService, IFirebasePushService firebasePushService)
        {
            _webSocketService = webSocketService;
            _scheduler = scheduler;
            _jobFactory = jobFactory;
            _notificationService = notificationService;
            _accountService = accountService;
            _firebasePushService = firebasePushService;
        }


        [HttpPost]
        [Route("startNotify")]
        public async Task<IActionResult> Sent([FromBody] TestModel model)
        {
            TimeConverter tc = new TimeConverter();


            var result = await _accountService.GetByPhoneNumber(model.PhoneNumber);

            WebSocketOutput wbout = new WebSocketOutput()
            {
                IsBlocked = model.IsBlocked,
                ParkingsTenant = model.ParkingsTenant,
                ParkingsOwner = model.ParkingsOwner,
                Payment = model.Payment,
                PhoneNumberChanged = model.PhoneNumberChanged
            };

        

            var json = JsonConvert.SerializeObject(wbout, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            await _webSocketService.Sent(result.Id, json);

            return Ok(json);
        }


        [HttpPost]
        [Route("SentFirebase")]
        public async Task<IActionResult> Sent([FromBody] FirebaseModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("WTF?");
            }
            var user = await _accountService.GetByPhoneNumber(model.PhoneNumber);

            if (user==null)
            {
                return BadRequest("invalid phone number");
            }

            if (user.PushToken.IsNullOrWhiteSpace())
            {
                return BadRequest("Push token is not set");
            }

            await _firebasePushService.SendPushNotification(user.PushToken, model.Message);

            return Ok();
        }
    }



   
}


public class FirebaseModel
{
    [Required]
    public string PhoneNumber { get; set; }
    [Required]
    public string Token { get; set; }
    [Required]
    public string Message { get; set; }
}

public class Notification
    {
        public List<int?> ParkingsTenant { get; set; }
        public List<int?> ParkingsOwner { get; set; }

        public bool IsPhoneNumberChanged { get; set; }
    }



    public class WebSocketInput
    {
        public string Token { get; set; }
    }


public class WebSocketOutput
{
    public bool IsBlocked { get; set; }
    public bool PhoneNumberChanged { get; set; }
    public List<int?> ParkingsOwner { get; set; }
    public List<int?> ParkingsTenant { get; set; }
    public List<int?> MyParkingPlaces { get; set; }
    public int? Payment { get; set; }


    public WebSocketOutput()
    {
        IsBlocked = false;
        PhoneNumberChanged = false;
        ParkingsTenant = new List<int?>();
        ParkingsOwner = new List<int?>();
        MyParkingPlaces = new List<int?>();
        Payment = 0;
    }
}


public class TestModel
{
    public string PhoneNumber { get; set; }
    public bool IsBlocked { get; set; }
    public bool PhoneNumberChanged { get; set; }
    public List<int?> ParkingsOwner { get; set; }
    public List<int?> ParkingsTenant { get; set; }
    public int? Payment { get; set; }
}