﻿using SlateCore.BLL.Interfaces.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Word = Microsoft.Office.Interop.Word;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// Class for working with docx documents
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    internal class DocxProvider<T>: IDocxProvider<T> where T: class
    {
        private readonly IValidationHelper _validationHelper;

        public DocxProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }

        object start = 0;
        object end = 0;
        object oNull = System.Reflection.Missing.Value;


        /// <summary>
        /// The method export objects to the docx file
        /// </summary>
        /// <param name="entities">Collection of objects</param>ё
        /// <param name="path">The path to the file</param>
        public void Export(IEnumerable<T> entities, string path)
        {
            _validationHelper.EntitiesValidate(entities);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            var columns = typeof(T).GetProperties().Length;
            var rows = entities.Count();
            var dataTable = Extenders.ToDataTable(entities, nameof(T));

            Word.Application application = new Word.Application();
            Word.Document document = application.Documents.Open(path);
            Word.Table table = document.Tables.Add(document.Range(ref start, ref end), rows + 1, columns, ref oNull, ref oNull);
            table.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            table.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            table.AllowAutoFit = true;

            //Headers table
            var counter = 0;
            foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
            {
                string columnName = propertyInfo.Name;
                table.Rows[0 + 1].Cells[counter + 1].Range.Text = columnName;
                counter++;
            }

            //Content table
            for (int i = 0 + 1; i < rows + 1; i++)
            {
                for (int j = 0 + 1; j < columns + 1; j++)
                {
                    table.Rows[i + 1].Cells[j].Range.Text = dataTable.Rows[i - 1].ItemArray[j - 1].ToString();
                }
            }

            application.ActiveDocument.SaveAs(path);
            application.Quit();
            document.Close();

            FinalReleaseComObject(application, document, table);
        }


        /// <summary>
        /// Unmanaged com object freeing method
        /// </summary>
        /// <param name="items">Com objects</param>
        private void FinalReleaseComObject(params object[] items)
        {
            foreach (var item in items)
            {
                Marshal.FinalReleaseComObject(item);
            }
        }
    }
}
