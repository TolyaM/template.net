﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ParkSolo.BLL.Interfaces;

namespace ParkSolo.WEB_2_1.Infastructure.Middleware
{
    public class WebSocketManager
    {
        private static ConcurrentDictionary<int, WebSocket> _sockets = new ConcurrentDictionary<int, WebSocket>();


        public WebSocket GetSocketById(int id)
        {
            return _sockets.FirstOrDefault(p => p.Key == id).Value;
        }


        public ConcurrentDictionary<int, WebSocket> GetAll()
        {
            return _sockets;
        }


        public int GetId(WebSocket socket)
        {
            return _sockets.FirstOrDefault(p => p.Value == socket).Key;
        }


        public int AddSocket(WebSocket socket, int id)
        {
            _sockets.TryAdd(id, socket);

            return id;
        }


        public async Task RemoveSocket(int id)
        {
            WebSocket socketToRemove;
            _sockets.TryRemove(id, out socketToRemove);

            if (socketToRemove.State==WebSocketState.Open)
            {
                await socketToRemove.CloseOutputAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                    statusDescription: "Closed by the WebSocketManager",
                    cancellationToken: CancellationToken.None);
            }

           
        }


        public async Task CloseAsync(WebSocket socket)
        {
            await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                statusDescription: "Closed by the WebSocketManager",
                cancellationToken: CancellationToken.None);
        }


        private string CreateConnectionId()
        {
            return Guid.NewGuid().ToString();
        }


        public async Task SendMessageToAllAsync(string message)
        {
            foreach (var pair in _sockets)
            {
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message);
            }
        }


        public async Task SendMessageAsync(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
            {
                return;
            }
                

            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.ASCII.GetBytes(message),
                    offset: 0,
                    count: message.Length),
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }
    }
}