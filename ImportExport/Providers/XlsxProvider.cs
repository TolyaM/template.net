﻿using Ganss.Excel;
using SlateCore.BLL.Helpers;
using SlateCore.BLL.Interfaces.Providers;
using System.Collections.Generic;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// Class for working with Microsoft Excel (works with old format - xsl)
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    internal class XlsxProvider<T>: IXlsxProvider<T> where T: class, new()
    {
        private readonly IValidationHelper _validationHelper;

        public XlsxProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }


        /// <summary>
        /// Method to import data from excel file to object
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>Deserialized objects</returns>
        public IEnumerable<T> Import(string path)
        {
            _validationHelper.PathValidate(path);

            var data = new ExcelMapper(path).Fetch<T>();
            return data;
        }


        /// <summary>
        /// The method of exporting objects to the xslx file
        /// </summary>
        /// <param name="entities">Collection of objects</param>
        /// <param name="path">The path to the file</param>
        public void Export(IEnumerable<T> entities, string path)
        {
            _validationHelper.EntitiesValidate(entities);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            new ExcelMapper().Save(path, entities, nameof(T), xlsx: true);
        }


        /// <summary>
        /// The method of exporting object to the xslx file
        /// </summary>
        /// <param name="entity">Object</param>
        /// <param name="path">The path to the file</param>
        public void Export(T entity, string path)
        {
            _validationHelper.EntityValidate(entity);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            new ExcelMapper().Save(path, new List<T>() { entity }, nameof(T), xlsx:true);
        }
    }
}
