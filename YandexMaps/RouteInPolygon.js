ymaps.ready(init);
var myMap;

function init() {
    myMap = new ymaps.Map('map',
            {
                center: [53.85, 27.55],
                zoom: 8
            },
            {
                searchControlProvider: 'yandex#search'
              }
    ),

        minskGeoObject = new ymaps.GeoObject({
                // Geometry Description.
                geometry: {
                    type: "Point",
                    coordinates: [53.893009, 27.567444]
                },
                properties: {
                    // Options.
                    // The label icon will stretch to fit its content.
                    preset: 'islands#blackStretchyIcon'
                }
        }),
        vitebskGeoObject = new ymaps.GeoObject({
            // Geometry Description.
            geometry: {
                type: "Point",
                coordinates: [55.242977, 30.250472]
            },
            properties: {
                // Options.
                // The label icon will stretch to fit its content.
                preset: 'islands#blackStretchyIcon',
            }
        }),

        myMap.geoObjects
            .add(minskGeoObject);
        myMap.geoObjects
            .add(vitebskGeoObject);

    var coordinatesPolygon = getCoordinateFromJson();
    var polygon = buildPolygone(coordinatesPolygon);
    DrawRoute(minskGeoObject.geometry._coordinates, vitebskGeoObject.geometry._coordinates, polygon);
}

function createPolygon() {
    // Create a polygon without vertices.
    var polygon = new ymaps.Polygon([], {}, {
        // Cursor in add new vertices mode.
        editorDrawingCursor: "crosshair",
        // Maximum allowed number of vertices.
        editorMaxPoints: 5,
        // Stroke color.
        strokeColor: '#76ff76',
        // Stroke width.
        strokeWidth: 1
    });

    return polygon;
}

function changeColorBorder(polygon) {
    // In the mode of adding new vertices, we change the color of the polygon stroke.
    var stateMonitor = new ymaps.Monitor(polygon.editor.state);
    stateMonitor.add("drawing", function (newValue) {
        polygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
    });

}

function getCoordinateFromJson() {
    var xhr = new XMLHttpRequest(); 
    xhr.open('GET', 'coordinates.json', false);
    xhr.send();
    if (xhr.readyState !== 4) {
        alert(xhr.responseText);
    } else {
        var coordinates = [];
        coordinates = JSON.parse(xhr.responseText);
        return coordinates;
    }
}


//The method builds polygone by coordinates
function buildPolygone(coordinates) {
    var polygon = new ymaps.Polygon(coordinates);
    myMap.geoObjects.add(polygon);

    return polygon;
}


//The method draws route, determines distance of route out polygone, in polygone and point of cross route and polygon
//coordinateStartPoint - start point of route
//coordinateFinishPoint - finish point of route

function DrawRoute(coordinateStartPoint, coordinateFinishPoint, polygon) {
    var coordinatesCrossPoint;      //point of cross route and polygon

    ymaps.route([coordinateStartPoint, coordinateFinishPoint], { mapStateAutoApply: true }).then(
        function (route) {
            var fullDistanceRoute = route.getLength();
            alert('Distance of route ' + fullDistanceRoute/1000+' km ');
            myMap.geoObjects.add(route);

            var path = route.getPaths();
            var pathsObjects = ymaps.geoQuery(path),
                arrayCoordinates = [];
            debugger 
            pathsObjects.each(function (path) {
                var countPointInRoute = path.geometry.getCoordinates();
                for (var i = 0, l = countPointInRoute.length; i < l; i++) {
                    arrayCoordinates.push({
                        type: 'Point',
                        coordinates: countPointInRoute[i]
                    });
                }
            });
            //debugger 
            startCoordinate = arrayCoordinates[0];                     //coordinate start point of route
            finishCoordinate = arrayCoordinates[arrayCoordinates.length - 1];     //coordinate finish point of route

            for (var i = 0; i < arrayCoordinates.length; i++) {
                var coordinatesPoint = arrayCoordinates[i];
                var isBelonging = checkOwership(polygon, coordinatesPoint);
                if (!isBelonging) {
                    coordinatesCrossPoint = arrayCoordinates[i];
                    break;
                }
            }

            calculateDistance(startCoordinate.coordinates, finishCoordinate.coordinates, coordinatesCrossPoint.coordinates);
        });
}


//The function checks: point belongs to polygon
//point is object of coordinates checkable point 
//return boolean value
function checkOwership(poligon, point) {
    var isBelong = poligon.geometry.contains(point.coordinates);
    return isBelong;
}


//The function calculate distances route in polygon and distance route out poligon
//startCoordinate and coordinatesCrossPoint - points of 
function calculateDistance(startCoordinate, finishCoordinate, coordinatesCrossPoint) {
    ymaps.route([startCoordinate, coordinatesCrossPoint], { mapStateAutoApply: true }).then(function (route) {
        var distanceIn = route.getLength();
        alert('Distance to cross point ' + distanceIn/1000+' km ');
    });

    ymaps.route([coordinatesCrossPoint, finishCoordinate], { mapStateAutoApply: true }).then(function (route) {
        var distanceOut = route.getLength();
        alert('Distance after cross point ' + distanceOut/1000+' km ');
    });

}