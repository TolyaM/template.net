﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;


namespace ParkSolo.WEB_2_1.Helpers
{
    public interface IImageProcessor
    {
        IEnumerable<string> SupportedMimeTypes { get; }
        string GetImageMimeType(byte[] stream);
        string GetImageExtension(byte[] stream);
        bool CheckFileType(IEnumerable<IFormFile> files);
    }
}