﻿using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using ParkSolo.WEB_2_1.Util;
using Serilog;
using Newtonsoft.Json;


namespace ParkSolo.WEB_2_1.Services
{

    /// <summary>
    /// This class implements sending notifications to user devices by Firebase API
    /// </summary>
    public class FirebasePushService : IFirebasePushService
    {
        private  HttpClient _httpClient;

        public FirebasePushService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }


        /// <summary>
        /// Send notification to one device
        /// </summary>
        /// <param name="token">device token</param>
        /// <param name="message">Mesage</param>
        /// <returns>Status code</returns>
        public async Task<bool> SendPushNotification(string token, string message)
        {
            // We can customize time-to-live in seconds and other propetries.
            // For more information about JSON structure https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages
            var payload = new
            {
                to = token,
                notification = new
                {
                    body = message,
                    title = "ParkSolo"
                },
            };

            var response = await _httpClient.PostAsync("send", payload, new JsonMediaTypeFormatter());

            return response.IsSuccessStatusCode;
        }


        /// <summary>
        /// Send notifications to multiple devices
        /// </summary>
        /// <param name="tokens">The user's devices tokens</param>
        /// <param name="message">Mesage</param>
        /// <returns>Status code</returns>
        public async Task<bool> SendPushNotification(string[] tokens, string message)
        {
            // We can customize time-to-live in seconds and other propetries.
            // For more information about JSON structure https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages

            var payload = new
            {
                registration_ids = tokens,
                notification = new
                {
                    body = message,
                    title = "ParkSolo"
                },
            };

            var response = await _httpClient.PostAsync("send", payload, new JsonMediaTypeFormatter());

            return response.IsSuccessStatusCode;
        }


        /// <summary>
        /// The method sends the push (type - data) to one device
        /// </summary>
        /// <param name="token">The user's devices tokens</param>
        /// <param name="_data"></param>
        /// <returns></returns>
        public async Task<bool> SendPushData(string token, object _data)
        {
            //var payload = new
            //{
            //    to = token,
            //    data = _data
            //};

            var payload = new
            {
                to = token,
                notification = new
                {
                    body = "Новый заказ на аренду Вашего парковочного места",
                    title = "ParkSolo"
                },
                data = _data,
                content_available = true,
                priority = "high"
            };

            var response = await _httpClient.PostAsync("send", payload, new JsonMediaTypeFormatter());

            return response.IsSuccessStatusCode;
        }

    } 
}