﻿using SlateCore.BLL.Interfaces.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// Class for working with txt documents
    /// </summary>
    /// <typeparam name="T">Type</typeparam>
    internal class TxtProvider<T>: ITxtProvider<T> where T: class
    {
        private readonly IValidationHelper _validationHelper;

        public TxtProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }


        /// <summary>
        /// The method of exporting objects to the txt file
        /// </summary>
        /// <param name="entities">Collection of objects</param>
        /// <param name="path">The path to the file</param>
        public void Export(IEnumerable<T> entities, string path)
        {
            _validationHelper.EntitiesValidate(entities);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
            {
                for (int i = 0; i < entities.Count(); i++)
                {
                    foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
                    {
                        var proteptyValue = GetPropertyValue(entities.ElementAt(i), propertyInfo.Name);
                        file.WriteLine(proteptyValue);
                    }
                }

                file.Close();
            }
        }


        /// <summary>
        /// The method of exporting objects to the txt file
        /// </summary>
        /// <param name="entity">Object</param>
        /// <param name="path">The path to the file</param>
        public void Export(T entity, string path)
        {
            _validationHelper.EntityValidate(entity);
            _validationHelper.PathValidate(path);
            _validationHelper.CreateNonExistingFile(path);      

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
            {
                foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
                {
                    var proteptyValue = GetPropertyValue(entity, propertyInfo.Name);
                    file.WriteLine(proteptyValue);
                }

                file.Close();
            }
        }


        /// <summary>
        /// The method of obtaining value through reflection
        /// </summary>
        /// <param name="source">Object instance</param>
        /// <param name="propertyName">Property name</param>
        /// <returns></returns>
        private object GetPropertyValue(object source, string propertyName)
        {
            var property = source.GetType().GetRuntimeProperties()
                .FirstOrDefault(p => string.Equals(p.Name, propertyName, StringComparison.OrdinalIgnoreCase));
            return property?.GetValue(source);
        }
    }
}
