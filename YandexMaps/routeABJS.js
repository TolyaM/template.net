// ������� ymaps.ready() ����� �������, �����
// ���������� ��� ���������� API, � ����� ����� ����� ������ DOM-������.
ymaps.ready(init);

function init() {
    // �������� �����.    
    var myMap = new ymaps.Map("map",
        {
            // ���������� ������ �����.
            // ������� �� ���������: �������, �������.
            // ����� �� ���������� ���������� ������ ����� �������,
            // �������������� ������������ ����������� ���������.
            center: [53.90, 27.56],
            //53.904847, 27.564540
            // ������� ���������������. ���������� ��������:
            // �� 0 (���� ���) �� 19.
            zoom: 7,
            controls: ['routePanelControl']
        });

    var control = myMap.controls.get('routePanelControl');

    // ������� ��������� ������ ��� ���������� ��������.
    control.routePanel.state.set({
        // ��� �������������.
        type: 'masstransit',
        // �������� ����������� �������� ����� ����������� � ���� �����.
        fromEnabled: true,
        // ����� ��� ���������� ������ �����������.
        //from: '�����, ���������� 14',
        // ������� ����������� �������� ����� ���������� � ���� �����.
        toEnabled: true
            // ����� ��� ���������� ������ ����������.
        //to: '���������'
    });

    // ������� ����� ������ ��� ���������� ��������.
    control.routePanel.options.set({
        // ��������� ����� ������, ����������� ������ ������� ��������� � �������� ����� ��������.
        allowSwitch: false,
        // ������� ����������� ������ �� ����������� �����.
        // ����� ����� ������������� ������������� � ���� ����� �� ������, � ����� � ������� ����� ��������.
        reverseGeocoding: true,
        // ������� ���� �������������, ������� ����� �������� ������������� ��� ������.
        types: { masstransit: true, pedestrian: true, taxi: true }
    });

    // ������� ������, � ������� ������� ������������ ������ ������ ������� ��������� � �������� ����� ��������.
    //var switchPointsButton = new ymaps.control.Button({
    //    data: { content: "�������� �������", title: "�������� ����� �������" },
    //    options: { selectOnClick: false, maxWidth: 160 }
    //});
    //// ��������� ���������� ��� ������.
    //switchPointsButton.events.add('click', function () {
    //    // ������ ������� ��������� � �������� ����� ��������.
    //    control.routePanel.switchPoints();
    //});
    //myMap.controls.add(switchPointsButton);
}