﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Archive.BLL.DTO;
using Archive.BLL.Interfaces;
using Archive.Common.Enums;
using Archive.PL.Helpers;
using Archive.PL.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Archive.PL.Controllers
{
    [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme, Policy = "RoleManager")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;
        private readonly IOptions<AppSettings> _options;
        private readonly IMapper _mapper;
        private readonly IPermissionService _permissionService;
        private readonly PaginationHelper<RoleDTO> _rolesPaginationHelper = new PaginationHelper<RoleDTO>();
        private readonly SortingHelper _sortingHelper = new SortingHelper();
        private readonly UserHelper _userHelper = new UserHelper();


        public RoleController(IRoleService roleService, IOptions<AppSettings> options, IMapper mapper, IPermissionService permissionService)
        {
            _roleService = roleService;
            _options = options;
            _mapper = mapper;
            _permissionService = permissionService;
        }

        private IEnumerable<RoleDTO> FindUsersByFilter(string filterParam, string filterSearch, IEnumerable<RoleDTO> roles)
        {
            string normalizedFilterString = filterSearch.ToUpperInvariant();
            switch (filterParam)
            {
                case "Id":
                    return roles.Where(s => s.Id.ToString().ToUpperInvariant().Contains(normalizedFilterString));
                case "Name":
                    return roles.Where(s => s.Name.ToUpperInvariant().Contains(normalizedFilterString));

                case "CreatedAt":
                    return roles.Where(s => s.CreatedAt.ToString().ToUpperInvariant().Contains(normalizedFilterString));

                default: return roles;
            }
        }


        private IEnumerable<RoleDTO> FindRolesBySearchParameter(string search, IEnumerable<RoleDTO> users)
        {
            string normalizedSearch = search.ToUpperInvariant();

            return users.Where(s =>
                s.Name.ToUpperInvariant().Contains(normalizedSearch)
                || s.CreatedAt.ToString().ToUpperInvariant().Contains(normalizedSearch));

        }

        /// <summary>
        /// Gets roles from database
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="order">Order state</param>
        /// <param name="searchString">Search string</param>
        /// <returns>Found roles for current company</returns>
        [HttpGet]
        public async Task<IActionResult> Index(int page = 1, RoleSortState order = RoleSortState.IdAsc, string searchString = "", string filterParam = "", string filterSearch = "")
        {
            //TODO: VIEW
            if (searchString == null)
            {
                searchString = "";
            }

            var companyId = _userHelper.GetCompanyId(User);

            var roles = (await _roleService.GetAll(companyId));

            if (String.IsNullOrWhiteSpace(filterParam) || String.IsNullOrWhiteSpace(filterSearch))
            {
                roles = FindRolesBySearchParameter(searchString, roles);
            }
            else
            {
                roles = FindUsersByFilter(filterParam, filterSearch, roles);
            }

            var sortedRoles = _sortingHelper.SortingRoles(roles, order);

            var pageRoles = _rolesPaginationHelper.CreatePagination(sortedRoles, _options.Value.PageSize, page, out var count);

            var formedRoleIndexViewModel = new RoleIndexViewModel()
            {
                PageViewModel = new PageViewModel(count, page, _options.Value.PageSize),
                RoleSortViewModel = new RoleSortViewModel(order),
                RoleViewModels = _mapper.Map<IEnumerable<RoleDTO>, IEnumerable<RoleViewModel>>(pageRoles),
                SearchParameter = searchString,
                FilterParameter = filterParam,
                FilterSearch = filterSearch
            };

            return View(formedRoleIndexViewModel);
        }



        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var roleDto = await _roleService.GetRoleWithPermissions(id);
            var roleViewModel = _mapper.Map<RoleFullDTO, RoleEditViewModel>(roleDto);

            var permissions = await _permissionService.GetAll();
            var permissionsViewModel = _mapper.Map<IEnumerable<PermissionDTO>, IEnumerable<PermissionViewModel>>(permissions).ToList();

            foreach (var item in permissionsViewModel)
            {
                var permission = roleViewModel.Permissions.FirstOrDefault(s => s.Name == item.Name);

                if (permission!=null)
                {
                    item.Selected = true;
                }
            }

            roleViewModel.Permissions = permissionsViewModel;

            return View(roleViewModel);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(RoleEditViewModel role)
        {
            if (!ModelState.IsValid)
            {
                return View(role);
            }

            var roleEditDto = _mapper.Map<RoleEditViewModel, RoleEditDTO>(role);
            await _roleService.Update(roleEditDto);

            return RedirectToAction("Index", "Role");
        }


        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var permissions = await _permissionService.GetAll();
            var permissionsViewModel = _mapper.Map<IEnumerable<PermissionDTO>, IEnumerable<PermissionViewModel>>(permissions).ToList();
            RoleCreateViewModel roleCreateViewModel = new RoleCreateViewModel()
            {
                Permissions = permissionsViewModel
            };
            return View(roleCreateViewModel);
        }


        [HttpPost]
        public async Task<IActionResult> Create(RoleCreateViewModel model)
        { 
            var companyId = _userHelper.GetCompanyId(User);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var roleDto = _mapper.Map<RoleCreateViewModel, RoleCreateDTO>(model);
            roleDto.CompanyId = companyId;

            await _roleService.Create(roleDto);

            return RedirectToAction("Index", "Role");
        }


        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            await _roleService.Delete(id);

            return RedirectToAction("Index", "Role");
        }
    }
}