//origin - start point of route
//destination - end point of route  
//selectedMode - mode of route



var map;
var currentImg = 'D:/template/google map/result/currentPosition.png';
var currentLocation;
var polygonCoordinates;
var bounds;
var infoWindow;
var zoomValue = 8;  //level of scalability


//The method loads the map with center in current location, with drawing manager(for drawing polygone on map), with event listner for polygon (polygoncomplete).
function initMap() {
    infowindow = new google.maps.InfoWindow();


    var getRoute = document.getElementById('routeId');

    //set coordinates center map as current coordinates
    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(function (position) {
            currentLocation = new google.maps.LatLng({ lat: position.coords.latitude, lng: position.coords.longitude });

            var marker = new google.maps.Marker({
                map: map,
                position: currentLocation,
                icon: currentImg
            });
            map.setCenter(currentLocation);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    }
    else {
        handleLocationError(false, infoWindow, map.getCenter());
    }

    map = new google.maps.Map(document.getElementById('map'), {
        center: currentLocation,
        zoom: zoomValue
    });

    //get drawing manager and set it on map
    var drawingManager = setDrawingManager();
    drawingManager.setMap(map);


    google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
        var coordinates = JSON.stringify(polygon.getPath().getArray());
        saveCoordinates(coordinates, "coordinates.json");
        polygonCoordinates = polygon.getPath().getArray();
    });
    

    getRoute.addEventListener('click', function () {
        var coordinates = getCoordinateFromJson();
        bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < coordinates.length; i++) {
            createMarker(coordinates[i]);
            bounds.extend(coordinates[i]);
        }
        drawPolygon(coordinates, bounds);
    });
}

//The metod setups drawing manager
function setDrawingManager() {
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,    //defines shape for drawing 
        drawingControl: true,                                    //defines the visibility of the drawing tools on map
        drawingControlOptions: {                                 //defines options for the drawing tools
            position: google.maps.ControlPosition.TOP_CENTER,    //set position for drawing tools
            drawingModes: ['polyline']                           //set elements of drawing tools
        }
    });

    drawingManager.setMap(map);
    return drawingManager;
}

//The method reads polygons coordinates from json-file
//Return value is array of coordinates
function getCoordinateFromJson() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'coordinates.json', false);
    xhr.send();
    if (xhr.readyState !== 4) {
        alert(xhr.responseText);
    } else {
        var coordinates = [];
        coordinates = JSON.parse(xhr.responseText);
        return coordinates;
    }
}

//The method creates marker on map
//Parametr position is location of marker on map
function createMarker(position) {
    new google.maps.Marker({
        map: map,
        position: position
    });
}

//The method draws polygon and marker in center polygon on map
//Parametr coordinates is array of polygons coordinates
//Parametr bounds is rechtangle which contain polygon
function drawPolygon(coordinates, bounds) {
    //set style for polygon
    var polygon = new google.maps.Polygon({
        paths: coordinates,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: '#FF9933',
        fillOpacity: 0.35
    });
    polygon.setMap(map);

    //get center of polygon
    var centerPolygon = bounds.getCenter();
    createMarker(centerPolygon);
    //drawing route to centerPolygon
    drawRoute(centerPolygon, polygon);
}

//The method draws route from current location to center of polygon.Also, method count distance route in polygon and distance route out of polygon
//Parametr centerpolygone is coordinates of polygons center
//Parametr polygon is poligon on map
//finishPoint ��� � ������ ���������
function drawRoute(centerpolygone, polygon) {
    var mode = "DRIVING";
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map
    });
    directionsService.route({
        origin: currentLocation,
        destination: centerpolygone,
        travelMode: mode
    }
        , function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var length = response.routes[0].overview_path.length;
                var startPoint = response.routes[0].overview_path[0];
                var finishPoint = response.routes[0].overview_path[length - 1];
                var crossPointFirst;
                var crossPointSecond;
                var routeLength = response.routes[0].overview_path.length;

                for (var i = 0; i <= routeLength; i++) {
                    var latOfPoint = response.routes[0].overview_path[i].lat();
                    var ltnOfPoint = response.routes[0].overview_path[i].lng();
                    markerLatLng = new google.maps.LatLng({ lat: latOfPoint, lng: ltnOfPoint  });
                    var marker = new google.maps.Marker({
                        position: markerLatLng
                    });
                    var checkOwership = google.maps.geometry.poly.containsLocation(markerLatLng, polygon);
                    if (checkOwership) {
                        crossPointSecond = response.routes[0].overview_path[i];
                        crossPointFirst = response.routes[0].overview_path[i - 1];
                        break;
                    }

                }
                drawRouteBetweenPoints(startPoint, finishPoint, crossPointFirst, crossPointSecond);
            }
            else {
                windows.alert('Directions request failed due to ' + status);
            }
        });
}


//The method draws route which composed from 2 route
//Parameter start is coordinate first point on route
//Parameter finish is coordinate last point  on route
//Parameter crossPointFirst is coordinate of end first part of route
//Parameter crossPointSecond is coordinate of start second part of route
function drawRouteBetweenPoints(startRoute, finishRoute, crossPointFirst, crossPointSecond) {
    var directionsService = new google.maps.DirectionsService();
    directionsService.route({
        origin: startRoute,
        destination: crossPointFirst,
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            var distanceOutRoute = response.routes[0].legs[0].distance.value;
            alert(distanceOutRoute);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });

    directionsService.route({
        origin: crossPointSecond,
        destination: finishRoute,
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            var disatnceInRoute = response.routes[0].legs[0].distance.value;
            alert(disatnceInRoute);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}


//The method save text in json-file
//Parameter text is stored data
//Parameter filename is name of new file
function saveCoordinates(text, filename) {
    var a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click();
}

//The method handles error
//Parameter browserHasGeolocation is flag (browser supports geolocation)
//Parameter infoWindow is instance of InfoWindow (will contains error message)
//Parameter currentLocation is coordinates position of infoWindow
function handleLocationError(browserHasGeolocation, infoWindow, currentLocation) {
    infoWindow.setPosition(currentLocation);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

