﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// IEnumerable class extension. IEnumerable Table Conversion.
    /// </summary>
    public static class Extenders
    {
        /// <summary>
        /// The method of converting a collection into a table object with table name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection of objects</param>
        /// <param name="tableName">String value</param>
        /// <returns>DataTable object</returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string tableName)
        {
            DataTable dataTable = ToDataTable(collection);
            dataTable.TableName = tableName;

            return dataTable;
        }


        /// <summary>
        /// The method of converting a collection into a table object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">Collection of objects</param>
        /// <returns>DataTable object</returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dataTable = new DataTable();
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties();

            //Create the columns in the DataTable
            foreach (PropertyInfo propertyInfo in properties)
            {
                dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType);
            }

            //Populate the table
            foreach (T item in collection)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow.BeginEdit();

                foreach (PropertyInfo propertyInfo in properties)
                {
                    dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null);
                }

                dataRow.EndEdit();
                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }
    }
}
