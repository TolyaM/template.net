﻿using System.Threading.Tasks;

namespace ParkSolo.WEB_2_1.Services
{
    public interface IWebSocketService
    {

        Task Sent(int userId, string message, int eventId);
        Task Sent(int userId, string message);
    }
}