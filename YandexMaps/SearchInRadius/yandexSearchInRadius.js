// searchControl - control for search 
// circle - ymaps.Circle object
//The method searches for all points in a given radius.
function displaySearchResultInRadius(searchControl, circle){
    searchControl.events.add('load', function (event) {//Event response points from the server
        searchControl.hideResult();//Hiding the result
        searchObjects = new ymaps.geoQuery(searchControl.getResultsArray());//Convert the object to geoQueryObject     
        var objectsInsideCircle = searchObjects.searchInside(circle); //Objects that are included in the circle
        searchObjects.remove(objectsInsideCircle).setOptions('visible', false);//Hide the points outside the circle
    });
}
