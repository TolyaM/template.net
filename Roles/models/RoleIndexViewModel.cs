﻿using System.Collections.Generic;

namespace Archive.PL.Models
{
    public class RoleIndexViewModel
    {
        public PageViewModel PageViewModel { get; set; }
        public RoleSortViewModel RoleSortViewModel { get; set; }
        public IEnumerable<RoleViewModel> RoleViewModels { get; set; }
        public string SearchParameter { get; set; }
        public string FilterParameter { get; set; }
        public string FilterSearch { get; set; }
    }
}