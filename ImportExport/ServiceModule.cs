﻿using Ninject.Modules;
using SlateCore.BLL.Helpers;
using SlateCore.BLL.Infrastructure;
using SlateCore.BLL.Infrastructure.Providers;
using SlateCore.BLL.Interfaces.Providers;
using SlateCore.DAL.Interfaces;
using SlateCore.DAL.Repositories;


namespace SlateCore.BLL.Util
{
    /// <summary>
    /// The class for introduction of dependencies
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        private string connectionString;

        public ServiceModule(string connection)
        {
            connectionString = connection;
        }

        /// <summary>
        /// The method establishes dependecies.
        /// The UnitOfWork use as the object IUnitOfWork
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
            Bind<IValidationHelper>().To<ValidationHelper>();
            Bind(typeof(IXmlProvider<>)).To(typeof(XmlProvider<>));
            Bind(typeof(IXlsxProvider<>)).To(typeof(XlsxProvider<>));
            Bind(typeof(IDocxProvider<>)).To(typeof(DocxProvider<>));
            Bind(typeof(ITxtProvider<>)).To(typeof(TxtProvider<>));
            Bind(typeof(IPdfProvider<>)).To(typeof(PdfProvider<>));
            Bind<IPngProvider>().To<PngProvider>();
        }
    }
}
