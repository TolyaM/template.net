﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ParkSolo.BLL.DTO;
using ParkSolo.BLL.Infrastructure;
using ParkSolo.BLL.Interfaces;
using ParkSolo.Common.Enums;
using ParkSolo.WEB.Models;
using ParkSolo.WEB_2_1.Controllers;
using ParkSolo.WEB_2_1.Infastructure.Quartz;
using ParkSolo.WEB_2_1.Interfaces;
using ParkSolo.WEB_2_1.Models;
using Quartz;
using Quartz.Spi;
using System.Linq;


namespace ParkSolo.WEB_2_1.Services
{
    public interface INotificationManager
    {
        Task CreateNotificationAboutParkingRent(int userId, int parkingPlaceId);
        Task CreateNotificationToTenantBeforeThreeHour(int userId, int parkingPlaceId, int orderStart);
        Task CreateNotificationToOwnerBefore24Hour(int userId, int parkingPlaceId, int orderStart);
        Task CreateNotificationAboutRentBeenCancelled(int userId, int parkingPlaceId);

        Task CreateNotificationToOwnerBefore24HourForParkingAvailabilityExpire(int userId, int parkingPlaceId, DateTime availableLastDate);

        Task CreateNotificationToOwnerNewPayment(int userId);
        Task CreateNotificationBlockUserAccount(int userId);
        Task CreateNotificationAboutParkingRentForTenant(int parkingPlaceId, int tenantId, UserOrderViewModel order);
    }

    public class NotificationManager: INotificationManager
    {
        private readonly INotificationService _notificationService;
        private readonly IWebSocketService _webSocketService;
        private readonly IScheduler _scheduler;
        private readonly IJobFactory _jobFactory;
        private readonly IFirebasePushService _firebasePushService;
        private readonly IAccountService _accountService;
        private readonly IEmailSender _emailSender;
        private readonly IParkingPlaceService _parkingPlaceService;
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;


        public NotificationManager(INotificationService notificationService, IWebSocketService webSocketService, 
                                   IScheduler scheduler, IJobFactory jobFactory, IFirebasePushService firebasePushService, 
                                   IAccountService accountService, IEmailSender emailSender, IOrderService orderService,
                                   IParkingPlaceService parkingPlaceService, IMapper mapper)
        {
            _notificationService = notificationService;
            _webSocketService = webSocketService;
            _scheduler = scheduler;
            _jobFactory = jobFactory;
            _firebasePushService = firebasePushService;
            _accountService = accountService;
            _emailSender = emailSender;
            _parkingPlaceService = parkingPlaceService;
            _orderService = orderService;
            _mapper = mapper;
        }

        public async Task CreateNotificationAboutParkingRent(int userId, int parkingPlaceId)
        {
            TimeConverter timeConverter = new TimeConverter();

            var notification = new NotificationDTO()
            {
                EventStatus = EventStatus.Owner,
                Message = $"Ваше парковочное место было забронировано!",
                ParkingId = parkingPlaceId,
                UserId = userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(timeConverter.DateTimeToUnixSeconds(DateTime.Now))
            };
            await _notificationService.Create(notification);

            WebSocketOutput outputModel = new WebSocketOutput {ParkingsOwner = new List<int?>() {parkingPlaceId}};

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            var user = await _accountService.GetById(userId);

            await _webSocketService.Sent(userId, json, notification.Id);
            await _firebasePushService.SendPushNotification(user.PushToken, $"Ваша парковка была арендована");

            if (user.Email!=null)
            {
                await _emailSender.SendEmailAsync(user.Email, "ParkSolo", "Привет!");
            }
            
        }


        public async Task CreateNotificationAboutParkingRentForTenant(int parkingPlaceId, int tenantId,  UserOrderViewModel order)
        {
            TimeConverter timeConverter = new TimeConverter();
            var startRent = timeConverter.UnixSecondsToDateTime(order.StartDateTime);
            var endRent = timeConverter.UnixSecondsToDateTime(order.EndDateTime);

            var foundParkingPlaceDTO = await _parkingPlaceService.GetById(parkingPlaceId);
            var foundTenantAccountDTO = await _accountService.GetById(tenantId);
            var foundOwnerAccountDTO = await _accountService.GetById(foundParkingPlaceDTO.UserId);

            var foundOrderDTO = (await _orderService.GetUserOrdersTenant(order.UserId)).Where(x => x.Id == order.Id).FirstOrDefault();
            var foundOrderVM = _mapper.Map<OrderDetailViewModel>(foundOrderDTO);

            await _firebasePushService.SendPushData(foundOwnerAccountDTO.PushToken, foundOrderVM);

            if (foundTenantAccountDTO.Email != null)
            {
                string message = $"Здравствуйте, {foundOwnerAccountDTO.FirstName}. <br/>Ваше парковочное место {foundParkingPlaceDTO.Name} по адресу {foundParkingPlaceDTO.Address} " +
                                 $"забронировано с {startRent.ToString("dd.MM.yyyy HH:mm")} по {endRent.ToString("dd.MM.yyyy HH:mm")}. " +
                                 $"Заказчик: {foundTenantAccountDTO.FirstName} {foundTenantAccountDTO.LastName} {foundTenantAccountDTO.Email} {foundTenantAccountDTO.PhoneNumber} " +
                                 $"<br/>С Уважением, <br/>Администрация ParkSolo";
                await _emailSender.SendEmailAsync(foundOwnerAccountDTO.Email, "ParkSolo", message);
            }
        }


        public async Task CreateNotificationAboutRentBeenCancelled(int userId, int parkingPlaceId)
        {
            TimeConverter timeConverter = new TimeConverter();

            var notification = new NotificationDTO()
            {
                EventStatus = EventStatus.Owner,
                Message = $"Бронирование вашего парковочного места было отменено!",
                ParkingId = parkingPlaceId,
                UserId = userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(timeConverter.DateTimeToUnixSeconds(DateTime.Now))
            };
            await _notificationService.Create(notification);

            WebSocketOutput outputModel = new WebSocketOutput { ParkingsOwner = new List<int?>() { parkingPlaceId } };

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            var user = await _accountService.GetById(userId);

            await _webSocketService.Sent(userId, json, notification.Id);
            await _firebasePushService.SendPushNotification(user.PushToken, "Бронирование вашего парковочного места было отменено!");
            await _emailSender.SendEmailAsync(user.Email, "ParkSolo", "Бронирование вашего парковочного места было отменено!");
        }


        public async Task CreateNotificationToOwnerBefore24HourForParkingAvailabilityExpire(int userId, int parkingPlaceId, 
                                                                                            DateTime availableLastDateTime)
        {
            TimeConverter timeConverter = new TimeConverter();
            var availableLastDate = timeConverter.DateTimeToUnixSeconds(availableLastDateTime);


            var notificationTime = availableLastDate - 86400;
            var notificationTimeUnix = notificationTime - 10800;



            var notificationTimeBefore24Hour = timeConverter.UnixSecondsToDateTime(notificationTimeUnix);


            var notificationBefore24Hour = new NotificationDTO()
            {
                EventStatus = EventStatus.OwnerParkingExpire,
                Message = $"Ваше парковочное место станет не доступным через 24 часа!",
                ParkingId = parkingPlaceId,
                UserId = userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(notificationTime)
            };

            await _notificationService.Create(notificationBefore24Hour);

            WebSocketOutput outputModel = new WebSocketOutput { MyParkingPlaces = new List<int?>() { parkingPlaceId } };

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });


            var job = JobBuilder.Create<PushNotificationJob>()
                .WithIdentity(Guid.NewGuid().ToString())
                .Build();


            job.JobDataMap.Put("userId", userId);
            job.JobDataMap.Put("json", json);
            job.JobDataMap.Put("eventId", notificationBefore24Hour.Id);

            var triggername = $"{Guid.NewGuid().ToString()}.trigger";
            var trigger = TriggerBuilder.Create()
                .WithIdentity(triggername)
                .StartAt(notificationTimeBefore24Hour)
                .Build();

            await _scheduler.ScheduleJob(job, trigger);
        }


        public async Task CreateNotificationToOwnerNewPayment(int userId)
        {
            TimeConverter timeConverter = new TimeConverter();

            var notification = new NotificationDTO()
            {
                EventStatus = EventStatus.Sum,
                Message = $"Появилась новая сумма для вывода",
                ParkingId = 0,
                UserId = userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(timeConverter.DateTimeToUnixSeconds(DateTime.Now))
            };

            await _notificationService.Create(notification);

            WebSocketOutput outputModel = new WebSocketOutput { Payment = 1 };

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            await _webSocketService.Sent(userId, json, notification.Id);
        }


        public async Task CreateNotificationBlockUserAccount(int userId)
        {
            TimeConverter timeConverter = new TimeConverter();

            var notification = new NotificationDTO()
            {
                EventStatus = EventStatus.Owner,
                Message = $"Ваше парковочное место было забронировано!",
                ParkingId = null,
                UserId = userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(timeConverter.DateTimeToUnixSeconds(DateTime.Now))
            };
            await _notificationService.Create(notification);

            WebSocketOutput outputModel = new WebSocketOutput {IsBlocked = true};

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            var user = await _accountService.GetById(userId);

            await _webSocketService.Sent(userId, json, notification.Id);
        }


        public async Task CreateNotificationToTenantBeforeThreeHour(int userId, int parkingPlaceId, int orderStartTime)
        {
            var notificationTime = orderStartTime - 10800;
            var notificationTimeUnix = notificationTime - 10800;

            TimeConverter timeConverter = new TimeConverter();
            var notificationDateTimeForQuartz = timeConverter.UnixSecondsToDateTime(notificationTimeUnix);

            var notificationBeforeThreeHour = new NotificationDTO()
            {
                EventStatus = EventStatus.Tenant,
                Message = $"Аренда парковочного места начнется через 3 часа!",
                ParkingId = parkingPlaceId,
                UserId = userId,
                IsSocket = false,
                CreatedOn = notificationTime
            };

            await _notificationService.Create(notificationBeforeThreeHour);


            WebSocketOutput outputModel = new WebSocketOutput { ParkingsTenant = new List<int?>() { parkingPlaceId } };

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });


            var job = JobBuilder.Create<PushNotificationJob>()
                .WithIdentity(Guid.NewGuid().ToString())
                .Build();


            job.JobDataMap.Put("userId", userId);
            job.JobDataMap.Put("json", json);
            job.JobDataMap.Put("eventId", notificationBeforeThreeHour.Id);

            var triggername1 = $"{Guid.NewGuid().ToString()}.trigger";
            var trigger1 = TriggerBuilder.Create()
                .WithIdentity(triggername1)
                .StartAt(notificationDateTimeForQuartz)
                .Build();

            await _scheduler.ScheduleJob(job, trigger1);
        }


        public async Task CreateNotificationToOwnerBefore24Hour(int userId, int parkingPlaceId, int orderStart)
        {
            TimeConverter timeConverter = new TimeConverter();

            var notificationTime = orderStart - 86400;
            var notificationTimeUnix = notificationTime - 10800-10800;

            var notificationTimeBefore24Hour = timeConverter.UnixSecondsToDateTime(notificationTimeUnix);


            var notificationBefore24Hour = new NotificationDTO()
            {
                EventStatus = EventStatus.Owner,
                Message = $"Аренда парковочного места начнется через 24 часа!",
                ParkingId = parkingPlaceId,
                UserId =userId,
                IsSocket = false,
                CreatedOn = Convert.ToInt32(notificationTime)
            };

            await _notificationService.Create(notificationBefore24Hour);

            WebSocketOutput outputModel = new WebSocketOutput { ParkingsOwner = new List<int?>() { parkingPlaceId } };

            var json = JsonConvert.SerializeObject(outputModel, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });


            var job = JobBuilder.Create<PushNotificationJob>()
                .WithIdentity(Guid.NewGuid().ToString())
                .Build();


            job.JobDataMap.Put("userId", userId);
            job.JobDataMap.Put("json", json);
            job.JobDataMap.Put("eventId", notificationBefore24Hour.Id);

            var triggername = $"{Guid.NewGuid().ToString()}.trigger";
            var trigger = TriggerBuilder.Create()
                .WithIdentity(triggername)
                .StartAt(notificationTimeBefore24Hour)
                .Build();

            await _scheduler.ScheduleJob(job, trigger);
        }
    }
}
