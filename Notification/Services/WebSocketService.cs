﻿using System.Linq;
using System.Threading.Tasks;
using ParkSolo.BLL.Interfaces;
using ParkSolo.WEB_2_1.Infastructure.Middleware;

namespace ParkSolo.WEB_2_1.Services
{
    public class WebSocketService : IWebSocketService
    {
        private readonly INotificationService _notificationService;

        private WebSocketManager _webSocketManager { get; set; }


        public WebSocketService(INotificationService notificationService)
        {
            _notificationService = notificationService;
            _webSocketManager = new WebSocketManager();
        }


        public async Task Sent(int userId, string message, int eventId)
        {
            var userSocket = _webSocketManager.GetAll().FirstOrDefault(s => s.Key == userId);

            if (userSocket.Value != null)
            {
                await _webSocketManager.SendMessageAsync(userSocket.Value, message);
                var notification = await _notificationService.GetById(eventId);
                notification.IsSocket = true;
                await _notificationService.Update(notification);

            }
        }

        public async Task Sent(int userId, string message)
        {
            var userSocket = _webSocketManager.GetAll().FirstOrDefault(s => s.Key == userId);

            if (userSocket.Value != null)
            {
                await _webSocketManager.SendMessageAsync(userSocket.Value, message);
            }
        }
    }
}