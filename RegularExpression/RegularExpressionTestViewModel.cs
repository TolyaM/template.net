﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RegularExpression.Models
{
    public class RegularExpressionTestViewModel
    {
        [RegularExpression("\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}")]
        public string PhoneNumber { get; set; }
        [RegularExpression("^((\\+7|7|8)+([0-9]){10})$")] 
        public string RuPhoneNumber { get; set; }
        [RegularExpression("^(\\+375|80)(29|25|44|33)(\\d{3})(\\d{2})(\\d{2})$")] 
        public string ByPhoneNumber { get; set; }
        [RegularExpression("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$")]
        public string LightEmail { get; set; }
        [RegularExpression("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")]
        public string HardEmail { get; set; }
        [RegularExpression("^[a-zA-Zа-яА-Я ,.'-]{3,30}$")]
        public string LastName { get; set; }
        [RegularExpression("^[a-zA-Zа-яА-Я ,.'-]{2,30}$")]
        public string FirstName { get; set; }
        [RegularExpression("^(?!^0+$)[a-zA-Z0-9]{3,20}$")]//101AE24,01234 
        public string PassportNumber { get; set; }
        [RegularExpression("^[A-Z]{2}+[0-9]{3,20}$")] //MC3123322
        public string ByPasportNumber { get; set; } 
        [RegularExpression("(([A-Za-z]){2,3}(|-)(?:[0-9]){1,2}(|-)(?:[A-Za-z]){2}(|-)([0-9]){1,4})|(([A-Za-z]){2,3}(|-)([0-9]){1,4})")] //https://www.regextester.com/94181
        public string CarNumber { get; set; }
        [RegularExpression("^[a-zA-Zа-яА-Я]+$")]
        public string OnlyLetters { get; set; }
        [RegularExpression("^[a-zа-я]+$")]
        public string OnlyLettersLowercase { get; set; }
        [RegularExpression("^[A-ZА-Я]+$")]
        public string OnlyLettersUppercase { get; set; }
        [RegularExpression("^[0-9]+$")]
        public int OnlyNumbers { get; set; }
        [RegularExpression("^[а-яА-ЯёЁa-zA-Z$&+,:;=?@#|'<>.^*()%!-]{0,10}$")]
        public string OnlySymbolsAndSpecialCharactersTo10Characters { get; set; }   
        [RegularExpression("~^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\\.)+(?:ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\\.){3}(?!0|255)[0-9]{1,3})(?:/[a-z0-9.,_@%&?+=\\~/-]*)?(?:#[^ '\"&]*)?$~i")]
        public string Link { get; set; }
        [RegularExpression("^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$")]
        public string IP { get; set; }
        [RegularExpression("^(?:[[:xdigit:]]{2}([-:]))(?:[[:xdigit:]]{2}\\1){4}[[:xdigit:]]{2}$")]
        public string MacAddress { get; set; }
        [RegularExpression("(0[1-9]|[12][0-9]|3[01])[- .](0[1-9]|1[012])[- .](19|20)\\d\\d")] 
        public DateTime DateDdmmyy { get; set; }
        [RegularExpression("^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\\d\\d\\d\\d (20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d$")] //04-03-2018 23:19:53
        public DateTime DateDdMmYyyyHhMmSs { get; set; }
    }
}
