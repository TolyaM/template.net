﻿using BLL.Infastructure;

namespace BLL.Interfaces
{
    public interface IPasswordGenerator
    {
        string GeneratePassword(PasswordOptions passwordOptions);
    }
}