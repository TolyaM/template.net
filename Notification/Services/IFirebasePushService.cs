﻿using System.Threading.Tasks;

namespace ParkSolo.WEB_2_1.Services
{
    public interface IFirebasePushService
    {
        Task<bool> SendPushNotification(string token, string message);
        Task<bool> SendPushNotification(string[] token, string message);
        Task<bool> SendPushData(string token, object data);
    }
}