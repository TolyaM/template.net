    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    var currentLocation;    //coordinates current location
    var map;
    var currentImg = 'D:/template/google map/result/currentPosition.png';      //icon for tick current location
    var infowindow;
    
    //The method loads the map with center in current location, with drawing manager(for drawing polygone on map), with event listner for polygon (polygoncomplete).
    function initMap() {
        infowindow = new google.maps.InfoWindow();

        //set coordinates center map as current coordinates 
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function (position) {
                currentLocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                var marker = new google.maps.Marker({
                    map: map,
                    position: currentLocation,
                    icon: currentImg
                });
                map.setCenter(currentLocation);

                //tick all pharmacies on map
                getPharmacies(currentLocation);
            });
        }
        else
        {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

        map = new google.maps.Map(document.getElementById('map'), {
            center: currentLocation,
            zoom: 15
        });          
       
    }

    //The method returns list of pharmacies in radius of 500 metrs 
    //Parameter position is coordinate of point for wich returns list of pharmacies
    function getPharmacies(position) {
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
                    location: position,     //coordinates start point
                    radius: 500,            //radius searched place
                    type: ['pharmacy']      //type searched place
                }, callback);
    }

    //The method is callback method for method getPharmacies(position)
    function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
        }
    }

    //The method creates marker on map
    //Parametr position is location of marker on map
    function createMarker(place) {
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }
