﻿using Microsoft.Office.Interop.Word;
using MigraDoc.Rendering;
using SlateCore.BLL.Interfaces.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using MigrateDocument = MigraDoc.DocumentObjectModel;
using MigrateTable = MigraDoc.DocumentObjectModel.Tables;

namespace SlateCore.BLL.Infrastructure.Providers
{
    /// <summary>
    /// Class for working with pdf documents
    /// </summary>
    internal class PdfProvider<T>: IPdfProvider<T> where T: class 
    {
        private readonly IValidationHelper _validationHelper;

        public PdfProvider(IValidationHelper validationHelper)
        {
            _validationHelper = validationHelper;
        }


        /// <summary>
        /// Method to export image to pdf file
        /// </summary>
        /// <param name="imagePath">The path to the file</param>
        public void ExportImageToPdf(string imagePath)
        {
            _validationHelper.PathExist(imagePath);
            string extension = Path.GetExtension(imagePath);
            string outputPath = imagePath.Substring(0, imagePath.Length - extension.Length) + ".pdf";

            MigrateDocument.Document document = new MigrateDocument.Document();
            MigrateDocument.Section section = document.AddSection();
            MigrateDocument.Paragraph paragraph = section.AddParagraph();

            paragraph.AddImage(imagePath);

            PdfDocumentRenderer pfdRenderer = new PdfDocumentRenderer();
            pfdRenderer.Document = document;
            pfdRenderer.RenderDocument();
            pfdRenderer.PdfDocument.Save(outputPath);
        }


        /// <summary>
        /// Method of exporting a collection to a pdf table
        /// </summary>
        /// <param name="entities">Collection of objects</param>ё
        /// <param name="path">The path to the file</param>
        public void Export(IEnumerable<T> entities, string path)
        {
            MigrateDocument.Document document = new MigrateDocument.Document();
            MigrateDocument.Section section = document.AddSection();

            MigrateTable.Table table = section.AddTable();
            var dataTable = Extenders.ToDataTable(entities, nameof(entities));
            var rows = entities.Count();
            var columns = typeof(T).GetProperties().Length;

            table.Style = "Table";
            table.Borders.Width = 0.15;
            table.Rows.LeftIndent = 0;
            table.Format.Alignment = MigrateDocument.ParagraphAlignment.Center;
            section.PageSetup.Orientation = MigrateDocument.Orientation.Landscape;//Horizont Orientation

            MigrateTableAddColumns(table, columns);
            MigrateTableAddRows(table, rows);
            table.AddRow();

            //Headers table
            var counter = 0;
            foreach (PropertyInfo propertyInfo in typeof(T).GetProperties())
            {
                string columnName = propertyInfo.Name;
                table.Rows[0].Cells[counter].AddParagraph(columnName);
                counter++;
            }

            // Add content in table
            for (int i = 1; i < rows + 1; i++)
            {   
                for (int j = 0; j < columns; j++)
                {
                    table.Rows[i].Cells[j].AddParagraph(dataTable.Rows[i - 1].ItemArray[j].ToString());
                }
            }

            PdfDocumentRenderer pfdRenderer = new PdfDocumentRenderer();
            pfdRenderer.Document = document;
            pfdRenderer.RenderDocument();

            pfdRenderer.PdfDocument.Save(path);
        }


        /// <summary>
        /// The method of exporting document to the pdf file
        /// </summary>
        /// <param name="documentPath">The path to the file</param>
        /// <returns>Succes - true or false</returns>
        public bool ExportDocumentToPdf(string documentPath)
        {
            _validationHelper.PathExist(documentPath);

            string extension = Path.GetExtension(documentPath);
            string outputPath = documentPath.Substring(0, documentPath.Length - extension.Length) + ".pdf";
            Microsoft.Office.Interop.Word.Application application = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document document = application.Documents.Open(documentPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (document == null)
            {
                application.Quit();

                return false;
            }

            var exportSuccessful = true;
            try
            {
                document.ExportAsFixedFormat(outputPath, WdExportFormat.wdExportFormatPDF);
            }
            catch (Exception e)
            {
                exportSuccessful = false;
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                document.Close();
                application.Quit();

                FinalReleaseComObject(application, document);
            }
            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (File.Exists(outputPath))
            {
                Process.Start(outputPath);
            }

            return exportSuccessful;
        }


        /// <summary>
        /// The method of exporting workbook to the pdf file
        /// </summary>
        /// <param name="workbookPath">The path to the file</param>
        /// <returns>Succes - true or false</returns>
        public bool ExportWorkbookToPdf(string workbookPath)
        {
            // If either required string is null or empty, stop and bail out
            _validationHelper.PathExist(workbookPath);


            string extension = Path.GetExtension(workbookPath);
            string outputPath = workbookPath.Substring(0, workbookPath.Length - extension.Length) + ".pdf";

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                return false;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (Exception ex)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                FinalReleaseComObject(excelApplication, excelWorkbook);
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (File.Exists(outputPath))
            {
                Process.Start(outputPath);
            }

            return exportSuccessful;
        }


        /// <summary>
        /// Unmanaged com object freeing method
        /// </summary>
        /// <param name="items">Com objects</param>
        private void FinalReleaseComObject(params object[] items)
        {
            foreach (var item in items)
            {
                Marshal.FinalReleaseComObject(item);
            }
        }


        /// <summary>
        /// Method of adding columns to a table
        /// </summary>
        /// <param name="count">Number of records</param>
        private void MigrateTableAddColumns(MigrateTable.Table table, int count)
        {
            while (count-- > 0)
            {
                table.AddColumn("150");
            }
        }


        /// <summary>
        /// Method of adding rows to a table
        /// </summary>
        /// <param name="count">Number of records</param>
        private void MigrateTableAddRows(MigrateTable.Table table, int count)
        {
            while (count-- > 0)
            {
                table.AddRow();
            }
        }
    }
}
