﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Archive.PL.Resources;
using Microsoft.AspNetCore.Mvc;

namespace Archive.PL.Models
{
    public class RoleEditViewModel
    {
        [HiddenInput]
        public int Id { get; set; }
        [Display(Name = "RoleName", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "ErrorMessageRequired", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(maximumLength: 30, MinimumLength = 5, ErrorMessageResourceName = "ErrorMessageRoleLength", ErrorMessageResourceType = typeof(Resource))]
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }
}