﻿//1* We are using coordinates of polygon from map, but we can use coordinates from file-json. 
//In this way we should use method getCoordinateFromJson(), this method commented in end this script

//2* The distances can calculate if there is the only cross point of cross route and polygon. 
//First distance is distance of firstPoint -> cross point. 
//Second distance is distance of crossPoint -> second point. 


ymaps.ready(init);
var map;
var polygon;
var arrayCoordinates = [];
var startCoordinatesOfRoute;
var finishCoordinatesOfRoute;
var coordinatesStartPointOfRoute;
var coordinatesFinishPointOfRoute;
var coordinatesCrossPoint;
var newRoute;

var searchObjects = {};

document.addEventListener("DOMContentLoaded", loadMap);

function init() {
    map = new ymaps.Map("map", {
        center: [53.90, 27.56],
        zoom: 10,
        controls: ['routePanelControl']
    },
        {
            searchControlProvider: 'yandex#search'
        });

    circle = new ymaps.Circle([[53.90, 27.50], 2000], null, { draggable: false });//Create circle
    map.geoObjects.add(circle); //Add circle on map

    var searchControl = new ymaps.control.SearchControl({
        options: {
            float: 'right',
            floatIndex: 100,
            noPlacemark: true
        }
    });
    map.controls.add(searchControl);

    displaySearchResultInRadius(searchControl, circle);//The function handles event of searching
}


//The method works after loading map, adds listeners to buttons and calls relevant functions
function loadMap() {

    var buildPolygonButton = document.getElementById('idButtonDrawPolygon');
    buildPolygonButton.addEventListener('click', function () {
            polygon = drawPolygon(polygon);
        });

    var saveButton = document.getElementById('idButtonSaveCoordinate');
    saveButton.addEventListener('click', function () {
        saveCoordinatesOfPolygon(polygon);
    });

    var drawRouteButton = document.getElementById('idRouteThroughPolygon');
    drawRouteButton.addEventListener('click', function () {
        drawRouteInPolygone();
    });

    var checkRouteButton = document.getElementById('idButtonCheckRoute');
    checkRouteButton.addEventListener('click', function () {
        getCoordinatesOfRoutesPoint();
        var convertedCoordinatesOfCrossPoint = coordinatesCrossPoint.coordinates;
        calculateDistance(coordinatesStartPointOfRoute, convertedCoordinatesOfCrossPoint);
        calculateDistance(convertedCoordinatesOfCrossPoint, coordinatesFinishPointOfRoute);

    });

}




//THE METHODS FOR DRAWING POLYGON 


//The method draws polygon 
function drawPolygon(polygon) {
    polygon = createPolygon();
    map.geoObjects.add(polygon);
    changeColorBorder(polygon);
    polygon.editor.startDrawing();
    return polygon;
}


//The method creates polygon 
function createPolygon() {
    //Creating polygone without vertices
    var newPolygon = new ymaps.Polygon([], {}, {
        //Set cursor in state of adding new vertex
        editorDrawingCursor: "crosshair",
        // The maximum allowed number of vertices.
        editorMaxPoints: 5,
        // The color of border 
        strokeColor: '#76ff76',
        //The width of border
        strokeWidth: 1
    });

    return newPolygon;
}


//The method changes color border of polygon by drawing
function changeColorBorder(myPolygon) {
    // In the mode of adding new vertices, we change the color of the polygon stroke.
    var stateMonitor = new ymaps.Monitor(myPolygon.editor.state);
    stateMonitor.add("drawing", function (newValue) {
        myPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
    });
}




//THE METHODS FOR SAVING COORDINATES OF POLYGON 


//The method gets coordinates of polygon, converts them and then
//posts data to method for saved in file
function saveCoordinatesOfPolygon(polygon) {
    if (!polygon) {
        alert('There is not polygon on the map');
    } else {
        var coordinates = JSON.stringify(polygon.geometry._coordPath._coordinates);
        saveCoordinates(coordinates, "coordinates.json");
    }
   
}


//The method sets coordinates in json-files and downloads file
//filename is name of downloading file
//text is value of coordinates
//file saved to standart location (for example downloads) 
function saveCoordinates(text, filename) {
    var a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
    a.setAttribute('download', filename);
    a.click();
}




//THE METHODS FOR CHECKING ROUTE
//YOU CAN CHECK ROUTE WHICH CROSS POLYGONE IN 1 POINT (for more points this code does not work)
function drawRouteInPolygone() {
    if (!polygon) {
        alert('There is not polygon on the map');
    } else {
        addRoute();
        drawRoute();
    }
}



//The methods sets 2-points on map: Minsk and Vitebsk, draws route between them
//The methods  returns array points of routew
function addRoute() {
    minskGeoObject = new ymaps.GeoObject({
        // Geometry Description.
        geometry: {
            type: "Point",
            coordinates: [53.893009, 27.567444]
        },
        properties: {
            // Options.
            // The label icon will stretch to fit its content.
            preset: 'islands#blackStretchyIcon'
        }
    }),

    otherGeoObject = new ymaps.GeoObject({

        // Geometry Description.
        geometry: {
            type: "Point",
            coordinates: [53.940846, 27.606564]
        },
        properties: {
            // Options.
            // The label icon will stretch to fit its content.
            preset: 'islands#blackStretchyIcon',
        }
    }),

    map.geoObjects.add(minskGeoObject);

    map.geoObjects.add(otherGeoObject);

    startCoordinatesOfRoute = minskGeoObject.geometry._coordinates;
    finishCoordinatesOfRoute = otherGeoObject.geometry._coordinates;
}


//The method draws route by coordinates of 2 points and adds route on map
//The methods  returns array points of route
function drawRoute() {
    ymaps.route([startCoordinatesOfRoute, finishCoordinatesOfRoute], { mapStateAutoApply: true }).then(
        function (route) {
            map.geoObjects.add(route);
            newRoute = route;
        },
        function (error) {
            alert('Error!');
        }
    );
}


//The method gets coordinates of routes point and finds coordinates of cross point
function getCoordinatesOfRoutesPoint() {
    var path = newRoute.getPaths();
    var pathsObjects = ymaps.geoQuery(path);
    var pointsInRoute = pathsObjects._objects[0].geometry._coordPath._coordinates;
    var countPointsInRoute = pointsInRoute.length;

    pathsObjects.each(function (path) {
        for (var i = 0, l = countPointsInRoute; i < l; i++) {
            arrayCoordinates.push({
                type: 'Point',
                coordinates: pointsInRoute[i]
            });
        }
    });

    coordinatesStartPointOfRoute = pointsInRoute[0];
    coordinatesFinishPointOfRoute = pointsInRoute[countPointsInRoute - 1];
    searchCrossPointPolygonAndRoute();
}


//The method searchs point of cross route with polygon
//Returns coordinates of point crossing 
function searchCrossPointPolygonAndRoute() {
    var countPointsOfRoute = arrayCoordinates.length;
    for (var i = 0; i < countPointsOfRoute; i++) {
        var coordinatesPoint = arrayCoordinates[i];
        var isBelonging = checkOwershipPointToPolygon( coordinatesPoint);
        if (isBelonging) {
            coordinatesCrossPoint = arrayCoordinates[i];
            break;
        }
    }
}

//The methods checks owership point to polygon 
//Point is coordinates of test point
//Return boolean value: true if point belong to polygon, otherwise - false
function checkOwershipPointToPolygon(point) {
    var isBelong = polygon.geometry.contains(point.coordinates);
        return isBelong;
    }


//The method calculates distance between 2 points
//Returns value of distance in m 
function calculateDistance(coordinatesStartPoint, coordinatesFinishPoint) {
    var distance;
    distance = ymaps.coordSystem.geo.getDistance(coordinatesStartPoint, coordinatesFinishPoint);
    alert(distance);
}


//The method searches for all points in a given radius
// searchControl - control for search 
// circle - ymaps.Circle object
function displaySearchResultInRadius(searchControl, circle) {
    searchControl.events.add('load', function (event) {//Event response points from the server
        searchControl.hideResult();//Hiding the result
        searchObjects = new ymaps.geoQuery(searchControl.getResultsArray());//Convert the object to geoQueryObject     
        var objectsInsideCircle = searchObjects.searchInside(circle); //Objects that are included in the circle
        searchObjects.remove(objectsInsideCircle).setOptions('visible', false);//Hide the points outside the circle
    });
}

















//1
////The method gets coordinates from json-file
//function getCoordinateFromJson() {
//    var xhr = new XMLHttpRequest();
//    xhr.open('GET', 'coordinates.json', false);
//    xhr.send();
//    if (xhr.readyState !== 4) {
//        alert(xhr.responseText);
//    } else {
//        var coordinates = [];
//        coordinates = JSON.parse(xhr.responseText);
//        return coordinates;
//    }
//}


