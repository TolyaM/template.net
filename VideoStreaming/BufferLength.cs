﻿using System;

namespace SlateCORE.Common.Enums
{
    [Flags]
    public enum BufferLength
    {
        HD = 1572864,
        FullHD = 3145728,
        UltraHD = 6291456
    }
}
