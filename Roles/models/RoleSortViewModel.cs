﻿using Archive.Common.Enums;

namespace Archive.PL.Models
{
    public class RoleSortViewModel
    {
        public RoleSortState Id { get; }
        public RoleSortState Name { get; }
        public RoleSortState Current { get; }


        public RoleSortViewModel(RoleSortState order)
        {
            Id = order == RoleSortState.IdAsc ? RoleSortState.IdDesc : RoleSortState.IdAsc;
            Name = order == RoleSortState.NameAsc ? RoleSortState.NameDesc : RoleSortState.NameAsc;
            this.Current = order;
        }
    }
}