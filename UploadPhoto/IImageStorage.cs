﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ParkSolo.WEB_2_1.Helpers
{
    public interface IImageStorage
    {
        Task<string> SaveAsync(int parkingPlaceId, byte[] stream, string fileName);
        Task<byte[]> LoadAsync(string blobPath);
        int FilesCount(int parkingPlaceId);
        bool Exists(string blobPath);
    }
}